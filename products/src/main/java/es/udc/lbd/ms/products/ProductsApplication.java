package es.udc.lbd.ms.products;

import es.udc.lbd.ms.products.model.domain.Product;
import es.udc.lbd.ms.products.model.service.ProductService;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Lazy;

import java.math.BigDecimal;

@SpringBootApplication
public class ProductsApplication {

	@Autowired
	@Lazy
	ProductService productService;

	public static void main(String[] args) {
		SpringApplication.run(ProductsApplication.class, args);
	}

	@PostConstruct
	public void initDB() {
		Product p1 = new Product(null, "apple", new BigDecimal("1.30"), null, null);
		p1 = productService.create(p1);
		Product p2 = new Product(null, "orage", new BigDecimal("2.20"), null, null);
		p2 = productService.create(p2);
	}


}
