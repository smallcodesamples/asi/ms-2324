package es.udc.lbd.ms.products.model.service;


import es.udc.lbd.ms.products.model.domain.Product;
import es.udc.lbd.ms.products.model.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class ProductService {
  @Autowired
  private ProductRepository productRepository;

  public List<Product> findAll() {
    return productRepository.findAll();
  }

  public Product findById(Long id) {
    return productRepository.findById(id).orElse(null);
  }

  @Transactional(readOnly=false)
  public Product create(Product product) {
    return productRepository.save(product);
  }

  @Transactional(readOnly=false)
  public Product update(Product product) {
    return productRepository.save(product);
  }

  @Transactional(readOnly=false)
  public void deleteById(Long id) {
    productRepository.deleteById(id);
  }

}
