package es.udc.lbd.ms.hello.web;

import es.udc.lbd.ms.hello.model.Hello;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloResource {


    @GetMapping("/{name}")
    public Hello getAccount(@PathVariable String name) {
        return new Hello(name, "hello");
    }
}
