package es.udc.lbd.ms.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

@SpringBootApplication
public class S1Application {

    public static void main(String[] args) {
        SpringApplication.run(S1Application.class, args);
    }

}


