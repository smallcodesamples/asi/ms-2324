package es.udc.lbd.ms.bye.web;

import es.udc.lbd.ms.bye.model.Bye;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bye")
public class ByeResource {


    @GetMapping("/{name}")
    public Bye getAccount(@PathVariable String name) {
        return new Bye(name, "bye");
    }
}
