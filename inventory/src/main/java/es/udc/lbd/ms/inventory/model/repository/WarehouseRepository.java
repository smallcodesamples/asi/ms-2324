package es.udc.lbd.ms.inventory.model.repository;

import es.udc.lbd.ms.inventory.model.domain.Warehouse;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface WarehouseRepository extends CrudRepository<Warehouse, Long> {

    List<Warehouse> findAll();

    Warehouse findById(long id);


}