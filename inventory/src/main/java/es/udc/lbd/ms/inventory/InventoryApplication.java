package es.udc.lbd.ms.inventory;

import es.udc.lbd.ms.inventory.model.domain.Product;
import es.udc.lbd.ms.inventory.model.domain.Warehouse;
import es.udc.lbd.ms.inventory.model.repository.ProductRepository;
import es.udc.lbd.ms.inventory.model.repository.WarehouseRepository;
import es.udc.lbd.ms.inventory.model.service.InventoryService;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Lazy;

import java.math.BigDecimal;

@SpringBootApplication
public class InventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventoryApplication.class, args);
	}

	@Autowired
	@Lazy
	InventoryService inventoryService;

	@Autowired
	@Lazy
	WarehouseRepository warehouseRepository;

	@Autowired
	@Lazy
	ProductRepository productRepository;


	@PostConstruct
	public void initDB() {
		Product p1 = new Product(null, "apple");
		p1 = productRepository.save(p1);
		Product p2 = new Product(null, "orage");
		p2 = productRepository.save(p2);

		Warehouse w1 = new Warehouse(null, "W1");
		w1 = warehouseRepository.save(w1);
		Warehouse w2 = new Warehouse(null, "W2");
		w2 = warehouseRepository.save(w2);

		inventoryService.updateInventory(p1.getId(), w1.getId(), 50L);
		inventoryService.updateInventory(p2.getId(), w1.getId(), 50L);
		inventoryService.updateInventory(p1.getId(), w2.getId(), 100L);
		inventoryService.updateInventory(p2.getId(), w2.getId(), 10L);

	}

}
