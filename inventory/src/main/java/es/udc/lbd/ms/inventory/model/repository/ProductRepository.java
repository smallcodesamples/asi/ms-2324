package es.udc.lbd.ms.inventory.model.repository;

import es.udc.lbd.ms.inventory.model.domain.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long> {

  List<Product> findAll();

  Product findById(long id);


}
