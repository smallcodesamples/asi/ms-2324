package es.udc.lbd.ms.inventory.web;


import es.udc.lbd.ms.inventory.model.domain.Inventory;
import es.udc.lbd.ms.inventory.model.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/inventory")
public class InventoryController {
    
    @Autowired
    private InventoryService inventoryService;

    @GetMapping("/warehouse/{warehouseId}")
    public List<Inventory> findByWarehouse(@PathVariable Long warehouseId) {
        return inventoryService.findByWarehouse(warehouseId);
    }

    @GetMapping("/product/{productId}")
    public List<Inventory> findByProduct(@PathVariable Long productId) {
        return inventoryService.findByWarehouse(productId);
    }

    @PostMapping("/product/{productId}/sell")
    public Boolean sell(@PathVariable Long productId) {
        return inventoryService.sell(productId);
    }

}
