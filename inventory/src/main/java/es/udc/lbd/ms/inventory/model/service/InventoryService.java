package es.udc.lbd.ms.inventory.model.service;


import es.udc.lbd.ms.inventory.model.domain.Inventory;
import es.udc.lbd.ms.inventory.model.domain.Warehouse;
import es.udc.lbd.ms.inventory.model.repository.InventoryRepository;
import es.udc.lbd.ms.inventory.model.repository.ProductRepository;
import es.udc.lbd.ms.inventory.model.repository.WarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = false, rollbackFor = Exception.class)
public class InventoryService {
  @Autowired
  private InventoryRepository inventoryRepository;

  @Autowired
  private ProductRepository productRepository;

  @Autowired
  private WarehouseRepository warehouseRepository;

  public List<Inventory> findByWarehouse(Long idWarehouse) {
    return inventoryRepository.findByWarehouseId(idWarehouse);
  }

  public boolean sell(Long idProduct) {
    Inventory inventory = inventoryRepository.findByProductId(idProduct).stream().filter((i)->i.getAmount()>0).findAny().get();

    inventory.setAmount(inventory.getAmount()-1);
    inventoryRepository.save(inventory);
    return true;
  }

  public void updateInventory(Long idProduct, Long idWarehouse, Long units){
    Inventory inventory = inventoryRepository.findByWarehouseIdAndProductId(idWarehouse, idProduct);
    if (inventory == null) {
      inventory = new Inventory();
      inventory.setWarehouse(warehouseRepository.findById(idWarehouse).orElse(null));
      inventory.setProduct(productRepository.findById(idProduct).orElse(null));
      inventory.setAmount(units);
      inventoryRepository.save(inventory);
    } else {
      inventory.setAmount(inventory.getAmount() + units);
      inventoryRepository.save(inventory);
    }
  }

}
