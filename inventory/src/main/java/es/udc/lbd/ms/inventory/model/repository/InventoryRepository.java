package es.udc.lbd.ms.inventory.model.repository;

import es.udc.lbd.ms.inventory.model.domain.Inventory;
import es.udc.lbd.ms.inventory.model.domain.Warehouse;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InventoryRepository extends CrudRepository<Inventory, Long> {

    Inventory findByWarehouseIdAndProductId(Long warehouseId, Long productId);

    List<Inventory> findByWarehouseId(Long warehouseId);

    List<Inventory> findByProductId(Long productId);


}