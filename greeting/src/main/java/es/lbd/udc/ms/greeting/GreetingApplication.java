package es.lbd.udc.ms.greeting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.function.Supplier;

@RestController
@SpringBootApplication
@EnableFeignClients
public class GreetingApplication {

	@FeignClient(name = "hello")
	public interface HelloClient {
		@GetMapping(value = "/hello/{name}")
		Greeting getGreetingHello(@PathVariable String name);
	}

	@Autowired
	HelloClient client;

	@Autowired
	private CircuitBreakerFactory circuitBreakerFactory;

	public static class Greeting {
		private String name;
		private String message;

		public Greeting() {

		}

		public Greeting(String name, String message) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}

	@GetMapping("/greeting/{name}")
	public Greeting getGreeting(@PathVariable String name) {

		RestTemplate restTemplate = new RestTemplate();
		
		Greeting greeting = new Greeting();
		CircuitBreaker cb = circuitBreakerFactory.create("greetingBreaker");
		greeting = cb.<Greeting>run( () -> client.getGreetingHello(name),
				throwable -> new Greeting(name.toUpperCase(), "FILLER"));

		return greeting;
	}


	public static void main(String[] args) {
		SpringApplication.run(GreetingApplication.class, args);
	}

}
