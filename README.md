# ms-example

## Entorno

Se proporciona un fichero compose.yaml que permite ejecutar un conjunto de servicios en Docker compose

```
docker compose up
```

## Servicios individuales

Cada carpeta es una aplicación individual de spring boot. Puede ejecutarse ed forma individual mediante:

```
 ./mvnw spring-boot:run -Ddockerfile.skip=true
```

Los servicios inventory y products requieren editar la configuración de la base de datos en el application.yml correspondiente