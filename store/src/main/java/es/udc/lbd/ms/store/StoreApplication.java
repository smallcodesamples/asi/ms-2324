package es.udc.lbd.ms.store;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableFeignClients
public class StoreApplication {

	@FeignClient(name = "inventory")
	public interface InventoryClient {

		@PostMapping("/inventory/product/{productId}/sell")
		public boolean sell(@PathVariable Long productId);

	}


	@Autowired
	@Lazy
	private InventoryClient inventoryClient;

	@Autowired
	private CircuitBreakerFactory circuitBreakerFactory;

	@GetMapping("/store/buy/{productId}")
	public boolean makeSell(@PathVariable Long productId) {
		CircuitBreaker cb = circuitBreakerFactory.create("buy");
		boolean result = cb.run( () -> inventoryClient.sell(productId),
				throwable -> false);
		return result;
	}

	public static void main(String[] args) {
		SpringApplication.run(StoreApplication.class, args);
	}


}
